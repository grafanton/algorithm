package algorithm.java;

import java.util.ArrayList;
import java.util.Collections;

public class exercise11 {
    public static int binarySearch (ArrayList<Integer> arrayList, int item) {
        int low = 0;
        int high = arrayList.size() - 1;

        while (low <= high) {
            int mid = (int) Math.floor((low + high) / 2);
            int guess = arrayList.get(mid);

            if (guess == item) {
                return item;
            }
            else if (guess > item) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        int arrayLength = (int) (Math.random() * 10);

        for (int i = 0; i < arrayLength; i++) {
            arrayList.add((int) (Math.random() * 10));
        }

        Collections.sort(arrayList);

        for (int i = 0; i < arrayLength ; i++) {
            System.out.println(arrayList.get(i));
        }

        int item = (int)(Math.random() * 10);
        System.out.println("Найти " + item);
        System.out.println(binarySearch(arrayList, item));
    }
}
