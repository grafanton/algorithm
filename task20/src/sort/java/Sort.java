package sort.java;

import java.util.ArrayList;

public class Sort {

    public static int findSmallest (ArrayList<Integer> arrayList) {
        int smallIndex = 0;
        int small = arrayList.get(smallIndex);

        for (int i = 1; i < arrayList.size(); i++) {
            if (arrayList.get(i) < small) {
                small = arrayList.get(i);
                smallIndex = i;
            }
        }

        return smallIndex;
    }

    public static void selectionSort (ArrayList<Integer> arrayList) {
        ArrayList<Integer> newArrayList = new ArrayList<>();
        int len = arrayList.size();

        for (int i = 0; i < len; i++) {
            int smallIndex = findSmallest(arrayList);
            newArrayList.add(arrayList.get(smallIndex));
            arrayList.remove(smallIndex);
        }

        arrayList.addAll(newArrayList);
    }

    public static void initialization (ArrayList<Integer> arrayList) {
        for (int i = 0; i < 10; i++) {
            arrayList.add((int)(Math.random() * 20));
        }
    }

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        initialization(arrayList);
        System.out.println(arrayList);
        System.out.println();
        selectionSort(arrayList);
        System.out.println(arrayList);
    }

}
